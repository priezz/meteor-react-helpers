"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.createReducer = exports.createAction = exports.store = exports.actions = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _reduxAct = require("redux-act");

var _redux = require("redux");

var _actions = {};
var _reducers = {};

/* Initialize Redux store */
var _store = (0, _redux.createStore)((0, _reduxAct.createReducer)({}, 0), (typeof window === "undefined" ? "undefined" : _typeof(window)) === "object" && typeof window.devToolsExtension !== "undefined" ? window.devToolsExtension() : function (f) {
    return f;
});

exports.actions = _actions;
exports.store = _store;


var createActionAuto = function createActionAuto(reducerName, actionName, fn) {
    var actionType = (reducerName + "__" + actionName).replace(/([A-Z])/g, "_$1").toUpperCase();
    var reducer = _reducers[reducerName];

    /* Cleanup action if it already exists */
    var oldAction = _actions[reducerName][actionName];
    if (oldAction) {
        reducer.off(oldAction);
        delete _actions[reducerName][actionName];
        _reduxAct.types.remove(actionType);
    }

    /* Create action and assign it to the store to be able to call them directy with automatic dispatching */
    var action = (0, _reduxAct.createAction)(actionType).assignTo(_store);
    _actions[reducerName][actionName] = action;

    reducer.on(action, fn);
};
exports.createAction = createActionAuto;


var createReducerAuto = function createReducerAuto(reducerName, actions, defaultState) {
    // TODO: add some parameters check here

    if (!_reducers[reducerName]) {
        (function () {
            _reducers[reducerName] = (0, _reduxAct.createReducer)({}, typeof defaultState !== "undefined" ? defaultState : {});
            _actions[reducerName] = {};

            var appReducer = (0, _redux.combineReducers)(_reducers);
            var resetReducer = function resetReducer(state, action) {
                return appReducer(action.type === "@@RESET" ? undefined : state, action);
            };

            _store.replaceReducer(resetReducer);
        })();
    }
    var reducer = _reducers[reducerName];

    Object.keys(actions).map(function (actionName) {
        if (!actions.hasOwnProperty(actionName)) return;
        createActionAuto(reducerName, actionName, actions[actionName]);
    });

    return reducer;
};
exports.createReducer = createReducerAuto;
;

var _temp = function () {
    if (typeof __REACT_HOT_LOADER__ === 'undefined') {
        return;
    }

    __REACT_HOT_LOADER__.register(_actions, "_actions", "src/reducers.js");

    __REACT_HOT_LOADER__.register(_reducers, "_reducers", "src/reducers.js");

    __REACT_HOT_LOADER__.register(_store, "_store", "src/reducers.js");

    __REACT_HOT_LOADER__.register(createActionAuto, "createActionAuto", "src/reducers.js");

    __REACT_HOT_LOADER__.register(createReducerAuto, "createReducerAuto", "src/reducers.js");
}();

;