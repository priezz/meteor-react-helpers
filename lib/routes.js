"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.pageReload = exports.setHead = exports.setGlobalHead = exports.setRoutes = exports.initApp = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactHotLoader = require("react-hot-loader");

var _meteor = require("meteor/meteor");

var _kadiraDochead = require("meteor/kadira:dochead");

var _kadiraFlowRouter = require("meteor/kadira:flow-router");

var _reactMounter = require("react-mounter");

var _redboxReact = require("redbox-react");

var _redboxReact2 = _interopRequireDefault(_redboxReact);

var _reactRedux = require("react-redux");

var _reducers = require("./reducers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _routes = void 0;
var _head = {};
var _redux = false;
var _hot = false;

var consoleErrorReporter = function consoleErrorReporter(_ref) {
    var error = _ref.error;

    /* eslint no-console: ["error", { allow: [ "error", "log" ] }] */
    console.error(error);
    return _react2.default.createElement(_redboxReact2.default, { error: error });
};
consoleErrorReporter.propTypes = { error: _react.PropTypes.instanceOf(Error).isRequired };

var setRoute = function setRoute(_ref2) {
    var _class, _temp;

    var path = _ref2.path,
        _ref2$container = _ref2.container,
        container = _ref2$container === undefined ? "default" : _ref2$container,
        source = _ref2.source,
        _ref2$head = _ref2.head,
        head = _ref2$head === undefined ? {} : _ref2$head,
        _ref2$options = _ref2.options,
        options = _ref2$options === undefined ? {} : _ref2$options;

    if (typeof container === "string" && !source) return;

    var mountWithOptions = (0, _reactMounter.withOptions)({
        rootId: "react-root",
        /* Set a className in the form 'page-path-to-the-page' or `page-root` */
        rootProps: { className: "page-" + path.split(":")[0].replace(/\//g, "-").replace(/(^-|-$)/g, "") }
    }, _reactMounter.mount);

    var App = container;
    /* Is source provided, import App container from file */
    if (source && typeof container === "string") App = require(source)[container];

    var Root = (_temp = _class = function (_Component) {
        _inherits(Root, _Component);

        function Root() {
            _classCallCheck(this, Root);

            return _possibleConstructorReturn(this, (Root.__proto__ || Object.getPrototypeOf(Root)).apply(this, arguments));
        }

        _createClass(Root, [{
            key: "getChildContext",
            value: function getChildContext() {
                return { actions: _reducers.actions };
            }
        }, {
            key: "render",
            value: function render() {
                /* Is source provided, use AppContainer for hot-reload */
                var app = _react2.default.createElement(App, _extends({ errorReporter: consoleErrorReporter }, options));
                if (_redux) app = _react2.default.createElement(
                    _reactRedux.Provider,
                    { store: _reducers.store },
                    app
                );
                if (_hot && source && typeof container === "string") app = _react2.default.createElement(
                    _reactHotLoader.AppContainer,
                    null,
                    app
                );
                return app;
            }
        }]);

        return Root;
    }(_react.Component), _class.childContextTypes = {
        actions: _react.PropTypes.objectOf(_react.PropTypes.objectOf(_react.PropTypes.func))
    }, _temp);


    _kadiraFlowRouter.FlowRouter.route(path, {
        action: function action() {
            mountWithOptions(Root);

            if (!_kadiraDochead.DocHead) return;

            _kadiraDochead.DocHead.setTitle(head.title || _head.title || "");

            _head.meta.forEach(function (meta) {
                return _kadiraDochead.DocHead.addMeta(meta);
            });
            if (head.meta) head.meta.forEach(function (meta) {
                return _kadiraDochead.DocHead.addMeta(meta);
            });

            _head.links.forEach(function (link) {
                return _kadiraDochead.DocHead.addLink(link);
            });
            if (head.links) head.links.forEach(function (link) {
                return _kadiraDochead.DocHead.addLink(link);
            });

            _head.scripts.forEach(function (script) {
                return _kadiraDochead.DocHead.loadScript(script.src, { async: script.async || true }, script.callback);
            });
            if (head.scripts) head.scripts.forEach(function (script) {
                return _kadiraDochead.DocHead.loadScript(script.src, { async: script.async || true }, script.callback);
            });
        }
    });
};

var initApp = exports.initApp = function initApp(_ref3) {
    var _ref3$routes = _ref3.routes,
        routes = _ref3$routes === undefined ? {} : _ref3$routes,
        hotModule = _ref3.hotModule,
        _ref3$resetStateOnRel = _ref3.resetStateOnReload,
        resetStateOnReload = _ref3$resetStateOnRel === undefined ? false : _ref3$resetStateOnRel,
        _ref3$redux = _ref3.redux,
        redux = _ref3$redux === undefined ? false : _ref3$redux;

    _routes = [];
    _redux = redux;

    /* Initialize hot-reload if running in development env
       http://webpack.github.io/docs/hot-module-replacement.html#api 
    */
    if (hotModule) {
        _hot = true;
        hotModule.dispose(function () {
            return pageReload(!resetStateOnReload);
        });
        hotModule.accept();
    }

    /* Normalize routes array */
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
        for (var _iterator = Object.keys(routes)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var path = _step.value;

            if (routes[path].container || routes[path].source) _routes.push(_extends({ path: path }, routes[path]));
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }

    _routes.forEach(function (route) {
        return setRoute(route);
    });

    /* Initialize FlowRouter */
    if (_meteor.Meteor.isClient && !_kadiraFlowRouter.FlowRouter._initialized) _meteor.Meteor.startup(function () {
        return _kadiraFlowRouter.FlowRouter.initialize();
    });
};
// deprecated
var setRoutes = exports.setRoutes = function setRoutes(routes, hot) {
    return initApp({ routes: routes, hotModule: hot });
};

var setGlobalHead = exports.setGlobalHead = function setGlobalHead(head) {
    _head = {
        title: head.title,
        meta: head.meta || [],
        links: head.links || [],
        scripts: head.scripts || []
    };
};

// deprecated
var setHead = exports.setHead = setGlobalHead;

var pageReload = exports.pageReload = function pageReload() {
    var keepReduxState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

    if (!keepReduxState) {
        console.info("[meteor-react-helpers] Reloading page and resetting the Redux state...");
        _reducers.store.dispatch({ type: "@@RESET" });
    } else console.info("[meteor-react-helpers] Reloading page keeping the Redux state...");
    _routes.forEach(function (route) {
        return setRoute(route);
    });
    _kadiraFlowRouter.FlowRouter._current.route._action();
};

/* Pause FlowRouter initialization before all the routes are difined */
_kadiraFlowRouter.FlowRouter.wait();
;

var _temp2 = function () {
    if (typeof __REACT_HOT_LOADER__ === 'undefined') {
        return;
    }

    __REACT_HOT_LOADER__.register(_routes, "_routes", "src/routes.jsx");

    __REACT_HOT_LOADER__.register(_head, "_head", "src/routes.jsx");

    __REACT_HOT_LOADER__.register(_redux, "_redux", "src/routes.jsx");

    __REACT_HOT_LOADER__.register(_hot, "_hot", "src/routes.jsx");

    __REACT_HOT_LOADER__.register(consoleErrorReporter, "consoleErrorReporter", "src/routes.jsx");

    __REACT_HOT_LOADER__.register(setRoute, "setRoute", "src/routes.jsx");

    __REACT_HOT_LOADER__.register(initApp, "initApp", "src/routes.jsx");

    __REACT_HOT_LOADER__.register(setRoutes, "setRoutes", "src/routes.jsx");

    __REACT_HOT_LOADER__.register(setGlobalHead, "setGlobalHead", "src/routes.jsx");

    __REACT_HOT_LOADER__.register(setHead, "setHead", "src/routes.jsx");

    __REACT_HOT_LOADER__.register(pageReload, "pageReload", "src/routes.jsx");
}();

;