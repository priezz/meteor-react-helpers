import { createAction, createReducer, types } from "redux-act"
import { createStore, combineReducers } from "redux"

const _actions = {}
const _reducers = {}

/* Initialize Redux store */
const _store = createStore( 
    createReducer({}, 0), 
    typeof window === "object" && typeof window.devToolsExtension !== "undefined" ? window.devToolsExtension() : f => f 
)

export { _actions as actions, _store as store }


const createActionAuto = ( reducerName, actionName, fn ) => {
    const actionType = ( reducerName + "__" + actionName ).replace( /([A-Z])/g, "_$1" ).toUpperCase()
    const reducer = _reducers[ reducerName ]

    /* Cleanup action if it already exists */
    const oldAction = _actions[ reducerName ][ actionName ]
    if( oldAction ) {
        reducer.off( oldAction )
        delete _actions[ reducerName ][ actionName ]
        types.remove( actionType )
    }
    
    /* Create action and assign it to the store to be able to call them directy with automatic dispatching */
    const action = createAction( actionType ).assignTo( _store )
    _actions[ reducerName ][ actionName ] = action

    reducer.on( action, fn )
}
export { createActionAuto as createAction }


const createReducerAuto = ( reducerName, actions, defaultState ) => {
    // TODO: add some parameters check here
    
    if( !_reducers[ reducerName ] ) {
        _reducers[ reducerName ] = createReducer( {}, typeof defaultState !== "undefined" ? defaultState : {} )
        _actions[ reducerName ] = {}
        
        const appReducer = combineReducers( _reducers )
        const resetReducer = ( state, action ) => appReducer ( action.type === "@@RESET" ? undefined : state, action )
        
        _store.replaceReducer( resetReducer )
    }
    const reducer = _reducers[ reducerName ]

    Object.keys( actions ).map( actionName => {
        if( !actions.hasOwnProperty( actionName ) ) return
        createActionAuto( reducerName, actionName, actions[ actionName ] )
    } )
    
    return reducer
}
export { createReducerAuto as createReducer }
