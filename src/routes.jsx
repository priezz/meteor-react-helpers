import { AppContainer } from "react-hot-loader"
import { Meteor } from "meteor/meteor"
import { DocHead } from "meteor/kadira:dochead"
import { FlowRouter } from "meteor/kadira:flow-router"
import { mount, withOptions as mountOptions } from "react-mounter"
import { Component, PropTypes } from "react"
import Redbox from "redbox-react"
import { Provider } from 'react-redux'

import { actions, store } from "./reducers"


let _routes
let _head = {}
let _redux = false
let _hot = false


const consoleErrorReporter = ( { error } ) => {
    /* eslint no-console: ["error", { allow: [ "error", "log" ] }] */
    console.error( error )
    return <Redbox error={ error } />
}
consoleErrorReporter.propTypes = { error: PropTypes.instanceOf( Error ).isRequired }


const setRoute = ( { path, container = "default", source, head = {}, options = {} } ) => {
    if( typeof container === "string" && !source ) return
    
    const mountWithOptions = mountOptions( {
        rootId: "react-root",
        /* Set a className in the form 'page-path-to-the-page' or `page-root` */
        rootProps: { className: "page-" + path.split( ":" )[0].replace( /\//g, "-" ).replace( /(^-|-$)/g, "" ) }
    }, mount )
    
    let App = container
    /* Is source provided, import App container from file */
    if( source && typeof container === "string" ) App = require( source )[ container ]

    class Root extends Component {
        static childContextTypes = {
            actions: PropTypes.objectOf( PropTypes.objectOf( PropTypes.func ) )
        }
        getChildContext() {
            return { actions }
        }
        render() {
            /* Is source provided, use AppContainer for hot-reload */
            let app = <App errorReporter={ consoleErrorReporter } { ...options } />
            if( _redux )
                app = <Provider store={ store }>{ app }</Provider>
            if( _hot && source && typeof container === "string" )
                app = <AppContainer>{ app }</AppContainer>
            return app
        }
    }

    FlowRouter.route( path, { action() {
        mountWithOptions( Root )
        
        if( !DocHead ) return
        
        DocHead.setTitle( head.title || _head.title || "" )
        
        _head.meta.forEach( meta => DocHead.addMeta( meta ) )
        if( head.meta ) head.meta.forEach( meta => DocHead.addMeta( meta ) )
        
        _head.links.forEach( link => DocHead.addLink( link ) )
        if( head.links ) head.links.forEach( link => DocHead.addLink( link ) )
        
        _head.scripts.forEach( script => DocHead.loadScript( script.src, { async: script.async || true }, script.callback ) )
        if( head.scripts ) head.scripts.forEach( script => DocHead.loadScript( script.src, { async: script.async || true }, script.callback ) )
    } })
}


export const initApp = ( { routes = {}, hotModule, resetStateOnReload = false, redux = false } ) => {
    _routes = []
    _redux = redux

    /* Initialize hot-reload if running in development env
       http://webpack.github.io/docs/hot-module-replacement.html#api 
    */
    if( hotModule ) {
        _hot = true
        hotModule.dispose( () => pageReload( !resetStateOnReload ) )
        hotModule.accept()
    }

    /* Normalize routes array */
    for( let path of Object.keys( routes ) ) {
        if( routes[path].container || routes[path].source ) _routes.push( { path, ...routes[path] } )
    }
    _routes.forEach( route => setRoute( route ) )
    
    /* Initialize FlowRouter */
    if( Meteor.isClient && !FlowRouter._initialized ) Meteor.startup( () => FlowRouter.initialize() )
}
// deprecated
export const setRoutes = ( routes, hot ) => initApp( { routes, hotModule: hot } )


export const setGlobalHead = ( head ) => { _head = { 
    title: head.title, 
    meta: head.meta || [],
    links: head.links || [],
    scripts: head.scripts || [],
} }

// deprecated
export const setHead = setGlobalHead


export const pageReload = ( keepReduxState = true ) => {
    if( !keepReduxState ) {
        console.info( "[meteor-react-helpers] Reloading page and resetting the Redux state...")
        store.dispatch( { type: "@@RESET" } )
    } else
        console.info( "[meteor-react-helpers] Reloading page keeping the Redux state...")
    _routes.forEach( route => setRoute( route ) )
    FlowRouter._current.route._action()
}


/* Pause FlowRouter initialization before all the routes are difined */
FlowRouter.wait()
