import "react-hot-loader/patch" //  IMPORTANT - should  be imported before React

/* Needed for onTouchTap - http://stackoverflow.com/a/34015469/988941 */
import injectTapEventPlugin from "react-tap-event-plugin"
injectTapEventPlugin()

export * from "./routes"
export * from "./reducers"
